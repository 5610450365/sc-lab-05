package Book;


public class Student {

	private String name;
	private String ID;
	private String faculty;
	public Student(String name, String ID,String faculty) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.ID = ID;
		this.faculty = faculty;
	}
	
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	
	public String getID(){
		return ID;
	}
	public void setID(String ID){
		this.ID = ID;
	}
	
	public String getFaculty(){
		return faculty;
	}
	public void setFaculty(String faculty){
		this.faculty = faculty;
	}
		  
}


