package Book;


public class Book {
	private String namebook,years;
	private String status = "";
	
	
	public Book(String namebook, String years) {
		this.namebook = namebook;
		this.years = years;
		this.status = "";
	}
	
	public String getNamebook(){
		return namebook;
	}
	public void setNamebook(String namebook){
		this.namebook = namebook;
	}
	public String getYears(){
		return years;
	}
	public void setYears(String years){
		this.years = years;
	}
	public String getStatus(){
		return status;
	}
	public void setStatus(String status){
		this.status = status;
	}

}
